#!/usr/bin/env python3
# coding: utf-8

import sys
import uuid
import time
import argparse
import hashlib
from enum import Enum
from collections import namedtuple
from itertools import chain
from typing import List, \
    Dict,\
    Tuple


# Draw black or white case
BLACK = "\u001b[40m "
WHITE = "\u001b[47m "
END = "\u001b[0m"


def timeit(func: callable) -> callable:
    """ Decorator to help timing function computation"""
    def wrapper(*args: List, **kwargs: Dict) -> Tuple:
        t0 = time.time()
        out = func(*args, **kwargs)
        t1 = time.time()
        return out, t1 - t0
    return wrapper


Position = namedtuple("Position", ["x", "y"])


class State(Enum):
    """Cell state!"""
    alive = "alive"
    dead = "dead"


class Cell(object):

    """ Game of Life representation of a cell.
    cell.state [State]"""

    def __init__(self,
                 position: Position,
                 state: State = State.dead):
        self.id = uuid.uuid4()
        self.state = state
        self.next_state = state
        self.position = position
        self.neighbours = list()

    def __repr__(self) -> str:
        return f"({self.position.x}:{self.position.y}=>{self.state.value})"

    @property
    def output(self) -> str:
        """Helper for the stdout."""
        return BLACK if self.state is State.alive else WHITE

    def next(self) -> State:
        """Apply Game Of Live rules (John Conway) -> For a space that is 'populated':
        - Each cell with one or no neighbors dies, as if by solitude.
        - Each cell with four or more neighbors dies, as if by overpopulation.
        - Each cell with two or three neighbors survives.
        - For a space that is 'empty' or 'unpopulated'
        - Each cell with three neighbors becomes populated."""

        # Count alived neighbours
        alived = len(list(filter(lambda x: x.state is State.alive, self.neighbours)))  # noqa

        if self.state is State.dead:
            if alived == 3:
                self.next_state = State.alive
            else:
                self.next_state = State.dead
        elif self.state is State.alive:
            if alived == 2 or alived == 3:
                self.next_state = State.alive
            else:
                self.next_state = State.dead
        else:
            raise ValueError(f"Cell state must be {State.__members__.values()}, passed: {self.state}")  # noqa

        return self.next_state


class Grid(object):

    """Representation of the grid, eg. Space where cells live."""

    def __init__(self, width: int, height: int, grid: List[str]):
        self.width = width
        self.height = height
        self.cells = self.__create_grid(grid)
        self.alives = []

    def __hash__(self):
        # Build hash from uuid of alived cells
        unique_str = ":".join(str(cell.id) for cell in self.alives)
        _hash = hashlib.md5(unique_str.encode())
        # simply cast the hex representation of the hash (base 16)
        return int(_hash.hexdigest(), 16)

    def __iter__(self) -> iter:
        """Enable Grid to be iterate as if it loop over each cells by lines."""

        # Flatten the grid, eg. [[1,2,3],[4,5,6]] => [1,2,3,4,5,6]
        return iter(chain.from_iterable(self.cells))

    def __create_grid(self, grid: List[str]) -> List[List[Cell]]:
        """Create the grid and populated it with the form input."""

        cells = []

        # Fill up the grid
        for x in range(self.height):
            cells.append(list())
            for y in range(self.width):
                cell = Cell(state=State.dead, position=Position(x, y))
                cells[x].append(cell)

        # place the input form in the center of
        # the grid
        for y, row in enumerate(grid):
            for x, cell in enumerate(row):
                x_center = (self.width // 2) + x
                y_center = (self.height // 2) + y
                if cell == " ":
                    cells[y_center][x_center].state = State.dead
                elif cell == "x":
                    cells[y_center][x_center].state = State.alive
                else:
                    raise ValueError(f"Invalid input from file, char:{cell}")

        # complete neighborhood
        # TODO: must be improved in case grid is huge...
        for row_index, row in enumerate(cells):
            for column_index, cell in enumerate(row):
                neighbours = []
                for i in range(row_index - 1, row_index + 2):
                    for j in range(column_index - 1, column_index + 2):
                        try:
                            # nand operator
                            if not (i == row_index and j == column_index):
                                neighbours.append(cells[i][j])
                        except IndexError:
                            # catch index error in case the given cell position
                            # is in one of the grid corner or along one side
                            pass

                cell.neighbours = neighbours

        return cells


class Statistic(object):

    """Object to collects statistics about game of life cells."""

    def __init__(self):
        self.alived_cells = list()
        self.dead_cells = list()
        self.changed_cells = list()

    def add(self, cell: Cell) -> Cell:
        """Add cell to the according lists, alived or changed."""

        if cell.state is State.alive:
            self.alived_cells.append(cell)
        elif cell.state is State.dead:
            self.dead_cells.append(cell)

        if cell.state != cell.next_state:
            self.changed_cells.append(cell)

        return cell

    def reset(self) -> None:
        """Clear all statistics lists."""
        self.alived_cells.clear()
        self.dead_cells.clear()
        self.changed_cells.clear()


class GameOfLife(object):

    """Basic object to represents game of life, it
    manage the grid, the iteration, and keep traces of
    some statistics."""

    def __init__(self,
                 input_form: List[str],
                 width: int = 200,
                 height: int = 200,
                 iteration: int = 10,
                 frame_rate: float = 0.02,
                 debug: bool = False):
        self.grid = Grid(width, height, input_form)
        self.iteration = iteration
        self.frame_rate = frame_rate
        self.debug = debug
        self.time = 0
        self.cycle = 0
        self.time_statistics = list()
        self.cell_statistics = list()
        self.hash_statistics = set()
        self.current_statistics = Statistic()

    def run(self):
        for turn in range(self.iteration):
            _, timer = self.compute_next_round()
            self.time_statistics.append(timer)
            if self.debug:
                self.show()
                time.sleep(self.frame_rate)
            self.time += 1
            if self.cycle:
                break
        self.show()
        time.sleep(5)

    def hash_grid(self) -> None:
        """Compute the hash of the grid to check if there is
         a cyle in the form."""

        _hash = hash(self.grid)
        if _hash in self.hash_statistics:
            self.cycle = self.time
        self.hash_statistics.add(_hash)

    @timeit
    def compute_next_round(self):
        self.current_statistics.reset()
        for cell in self.grid:
            cell.next()
            self.current_statistics.add(cell)

        for cell in self.grid:
            cell.state = cell.next_state

        self.grid.alives = self.current_statistics.alived_cells
        self.hash_grid()
        self.cell_statistics.append(self.current_statistics)

    def show(self):
        """Std out the grid with cells."""
        grid = ""
        print(f"Time:{self.time:5}, computation time:{self.time_statistics[-1]:20}s, min:{min(self.time_statistics):20}, max:{max(self.time_statistics)}")  # noqa
        print(f"Cells: alive:{len(self.current_statistics.alived_cells)}, dead:{len(self.current_statistics.dead_cells)}, changed:{len(self.current_statistics.changed_cells)}")  # noqa
        print(f"Reproducted:{self.time + 1 - len(self.hash_statistics)} cycle:{self.cycle}")  # noqa
        grid += f"+{'-' * (self.grid.width )}+\n"
        grid += "\n".join(["|" + "".join(cell.output for cell in row) + f"{END}|" for row in self.grid.cells])  # noqa
        grid += f"\n+{'-' * (self.grid.width )}+"
        print(grid, end="\r")
        [sys.stdout.write("\033[F") for _ in range(len(self.grid.cells) + 4)]


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Game of Life")
    parser.add_argument("--filename", "-f", help="filename of the input figure", type=argparse.FileType('r'))  # noqa
    parser.add_argument("--iteration", "-i", help="number of iteration", default=10, type=int)  # noqa
    parser.add_argument("--width", "-w", help="grid width", type=int, default=40)  # noqa
    parser.add_argument("--height", help="grid height", type=int, default=40)  # noqa
    parser.add_argument("--debug", "-d", help="Debugging flag", type=int, default=1)  # noqa
    parser.add_argument("--frame-rate", "-fr", help="image frame rate", type=float, default=0.02)  # noqa
    args = parser.parse_args()

    game = GameOfLife(args.filename.read().split("\n"),
                      args.width,
                      args.height,
                      args.iteration,
                      args.frame_rate,
                      args.debug)
    game.run()
